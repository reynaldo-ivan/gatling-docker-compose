package sample

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class test extends Simulation {

	val httpProtocol = http
		.baseURL("http://localhost:8082")
		.proxy(Proxy("localhost", 8082).httpsPort(8082))
		.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
		.acceptHeader("*/*")
		.userAgentHeader("PostmanRuntime/6.4.1")

	val headers_0 = Map(
		"Postman-Token" -> "a633613e-33fb-4426-aadf-e85b61074b65",
		"accept-encoding" -> "gzip, deflate",
		"cache-control" -> "no-cache")

	val headers_1 = Map(
		"Postman-Token" -> "6cf70a62-f2a8-4635-95d8-4028d75d2d66",
		"accept-encoding" -> "gzip, deflate",
		"cache-control" -> "no-cache")

    val uri1 = "http://localhost:8082/test"

	val scn = scenario("test")
		.exec(http("request_0")
			.get("/test")
			.headers(headers_0)
			.resources(http("request_1")
			.get("/test")
			.headers(headers_1)))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}

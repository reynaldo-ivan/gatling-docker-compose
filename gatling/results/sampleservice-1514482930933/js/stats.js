var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "99010",
        "ok": "79164",
        "ko": "19846"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "31452",
        "ok": "31452",
        "ko": "30806"
    },
    "meanResponseTime": {
        "total": "3445",
        "ok": "2168",
        "ko": "8537"
    },
    "standardDeviation": {
        "total": "7552",
        "ok": "6199",
        "ko": "9941"
    },
    "percentiles1": {
        "total": "5",
        "ok": "4",
        "ko": "2069"
    },
    "percentiles2": {
        "total": "150",
        "ok": "34",
        "ko": "17002"
    },
    "percentiles3": {
        "total": "23151",
        "ok": "19487",
        "ko": "27015"
    },
    "percentiles4": {
        "total": "28381",
        "ok": "27991",
        "ko": "29010"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 68372,
        "percentage": 69
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 410,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10382,
        "percentage": 10
    },
    "group4": {
        "name": "failed",
        "count": 19846,
        "percentage": 20
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "138.669",
        "ok": "110.874",
        "ko": "27.796"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "99010",
        "ok": "79164",
        "ko": "19846"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "0",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "31452",
        "ok": "31452",
        "ko": "30806"
    },
    "meanResponseTime": {
        "total": "3445",
        "ok": "2168",
        "ko": "8537"
    },
    "standardDeviation": {
        "total": "7552",
        "ok": "6199",
        "ko": "9941"
    },
    "percentiles1": {
        "total": "5",
        "ok": "4",
        "ko": "2069"
    },
    "percentiles2": {
        "total": "150",
        "ok": "34",
        "ko": "17002"
    },
    "percentiles3": {
        "total": "23150",
        "ok": "19479",
        "ko": "27014"
    },
    "percentiles4": {
        "total": "28381",
        "ok": "27991",
        "ko": "29010"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 68372,
        "percentage": 69
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 410,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 10382,
        "percentage": 10
    },
    "group4": {
        "name": "failed",
        "count": 19846,
        "percentage": 20
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "138.669",
        "ok": "110.874",
        "ko": "27.796"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
